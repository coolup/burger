import React from 'react';
import CurrentPrice from '../components/CurrentPrice/CurrentPrice';
import Burger from '../components/Burger/Burger';
import Constructor from './Constructor';

class BurgerConstructor extends React.Component {

    state = {
        totalsum:20,
        ingredients: [
            {name:'Salad', price:5, id:1, count:0},
            {name:'Bacon', price:30, id:2, count:0},
            {name:'Cheese', price:20, id:3, count:0},
            {name:'Meat', price:50, id:4, count:0}
        ]
    }

    increase = (id) => {
        const index = this.state.ingredients.findIndex(x=>x.id === id);
        const ingr = {...this.state.ingredients[index]}
        ingr.count = ingr.count + 1;
    
        const ingredients = [...this.state.ingredients];
        ingredients[index] = ingr;
    
        var totalsum = this.state.totalsum;
        totalsum = totalsum + ingr.price;
        this.setState({totalsum, ingredients});
    }

    
    decrease = (id) => { 
        const index = this.state.ingredients.findIndex(x=>x.id === id);
        const ingr = {...this.state.ingredients[index]}
        if(ingr.count > 0)         
        {
            ingr.count = ingr.count - 1;
            
            var totalsum = this.state.totalsum;
            
            totalsum = totalsum - ingr.price;

            const ingredients = [...this.state.ingredients];
            ingredients[index] = ingr;

            this.setState({totalsum, ingredients});
        }            
    }

    getIngrCount = (id) => {
        const index = this.state.ingredients.findIndex(i => i.id === id);
        const ingredients = [...this.state.ingredients];
    
        return ingredients[index].count;
      }

    render() {
        return <div>
            <Constructor 
            saladCount={this.getIngrCount(1)} 
            baconCount={this.getIngrCount(2)}
            cheeseCount={this.getIngrCount(3)}
            meatCount={this.getIngrCount(4)}
          />
            <CurrentPrice value={this.state.totalsum}/>
            <Burger ingredients={this.state.ingredients} 
            increase={this.increase}
            decrease={this.decrease}/>
        </div>
    }

}

export default BurgerConstructor