import React from 'react';
import './BurgerConstructor.css';

const Constructor = (props) => {

    let salad = [];
    for (let i = 0; i < props.saladCount; i++) {
        salad.push(<div className="Salad" ></div>)
    }

    let cheese = [];
    for (let i = 0; i < props.cheeseCount; i++) {
        cheese.push(<div className="Cheese" ></div>)
    }

    let bacon = [];
    for (let i = 0; i < props.baconCount; i++) {
        bacon.push(<div className="Bacon" ></div>)
    }

    let meat = [];
    for (let i = 0; i < props.meatCount; i++) {
        meat.push(<div className="Meat" ></div>)
    }

    return (
        <div className="Burger">
            <div className="BreadTop">
                <div className="Seeds1"></div>
                <div className="Seeds2"></div>
            </div>
            {salad}
            {cheese}
            {bacon}
            {meat}
            <div className="BreadBottom"></div>
        </div>
    )

}

export default Constructor;