import React from 'react';
import "../Ingredient/Ingredients.css"


class CounterButton extends React.Component {
    render () {
        return <div>
            <button className="Button" onClick={this.props.click}> {this.props.name} </button>
        </div>
    }
}

export default CounterButton