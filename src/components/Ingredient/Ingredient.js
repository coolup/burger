import React from 'react';
import CounterButton from '../Buttons/CounterButton';

function Ingredient(props)  {
        return ( 
            <div className="cont">
                {props.name}  
                   <CounterButton name="Less" value={props.count} click={props.decrease}/>     
                   <CounterButton name="More" click={props.increase}/>     
                
            </div>
        )   
    
}

export default Ingredient