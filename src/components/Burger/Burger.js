import React from 'react';
import Ingredient from '../Ingredient/Ingredient';

const Burger = props => {
    return props.ingredients.map(ingr => {
        return <Ingredient
          key={ingr.id}
          name={ingr.name}
          count={ingr.count}
          increase={() => props.increase(ingr.id)}
          decrease={() => props.decrease(ingr.id)}
        />
      });
};

export default Burger